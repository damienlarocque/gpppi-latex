<!-- vim: spelllang=fr
-->

# GPPPI LaTeX

Gabarit LaTeX pour rédiger des documents dans le style du *Guide de
préparation des présentations d'un projet d'ingénierie à l'usage des étudiantes
et des étudiants de la faculté d'ingénierie de l'Université de Moncton (GPPPI)*.

## Utilisation

Il est recommandé d'utiliser le gabarit en tant que "submodule" Git:

```
git init
git submodule add -b master git@gitlab.com:IamPhytan/gpppi-latex.git
```

Par la suite, la classe de document peut être utilise comme suit:

```tex
% !TeX root = ./exemple.tex
% !TEX program = xelatex
% !TEX encoding = UTF-8 Unicode
% !TEX options = --shell-escape -synctex=1 -interaction=nonstopmode -file-line-error

\documentclass{./gpppi}
\usepackage{blindtext}

\begin{document}

\gpppititlepage
    {GELE1000}
    {Nom de cours}
    {Laboratoire 100}
    {Nom du travail}
    {Damien LaRocque (NI)}
    {Professeur d'université, Ing, Ph.D}
    {Le 1 janvier 1970}

\chapter{Exemple de section}
\blindtext{}
\chapter{Exemple de sous section}
\blindtext{}
\section{Exemple de sous sous section}
\blindtext{}

\end{document}
```

## Fonctions

### Page titre

La classe fournit la commande `\gpppititlepage` qui permet de générer
facilement une page titre dans le style de la faculté.

```tex
\gpppititlepage{<sigle>}{<nom du cours>}{<titre-1>}{<titre-2>}{<auteur> (<NI>)}{<prof>}{<date de remise>}
```

### Énumération des sections

Par défaut, le seul préfixe ajouté avant le texte fourni est le numéro de la
(sous) section (par exemple: "1.1.1. Titre de la section").

Pour une énumération comme dans un devoir, il est possible de fournir
l'option `sections` à l'appel de la classe:

```tex
\documentclass[relativepath=./gpppi-latex/, sections=Problème]{./gpppi-latex/gpppi}
...
\chapter{chapter}
\chapter{Sous section}
\section{Sous sous section}
```

Cela produira un document dans le style suivant:

```
Problème 1  Section
a)  Sous section
1.  Sous sous section
```

## Troubleshooting

### Fatal Package fontspec Error: The fontspec package requires either XeTeX or LuaTeX.

La classe utilise les polices du GPPPI (Arial et Calibri). Cela est fait avec
la librairie `fontspec`, qui exige XeTeX ou LuaTeX. Il faut ajouter les lignes
suivantes au-dessus de votre fichier pour utiliser un compilateur XeTeX :

```
% !TEX program = xelatex
% !TEX encoding = UTF-8 Unicode
```

### LaTeX Warning: You have requested document class `./gpppi-latex/gpppi`, but the document class provides `./gpppi-latexgpppi`.

Cette erreur se produit si la `documentclass` utilisée ne correspond pas à ce
que la classe fournit (la classe fournit `<relativepath>gpppi`). Cela peut se
produire si vous utilisez
`\documentclass[relativepath=./gpppi-latex]{./gpppi-latex/gpppi}` au lieu de
`\documentclass[relativepath=./gpppi-latex/]{./gpppi-latex/gpppi}` (notez l'absence de `/` à la fin de `relativepath`).
