%vim: ft=tex

\NeedsTeXFormat{LaTeX2e}

\RequirePackage{pgfkeys}
\RequirePackage{pgfopts}

\pgfkeys{%
	/gpppi/.is family,
	/gpppi,
	relativepath/.store in=\relativepath,
    relativepath={./},
    sections/.store in=\chapterprefix,
}

\ProcessPgfOptions{/gpppi}

% Hack to combine variable and text without any characters between
\newcommand{\gpppi}{gpppi}
\ProvidesClass{\relativepath\gpppi}[LaTeX class for GPPPI format]


% Page setup
% ========================================

\LoadClass[12pt, draft]{report}

% Page margins
\usepackage[top=25mm,right=25mm,bottom=25mm,left=40mm]{geometry}


% Language setup
% ========================================

\usepackage[english,french]{babel}
\usepackage[T1]{fontenc}

% Nom de tableau
\addto{\captionsfrench}{\renewcommand{\tablename}{Tableau}}


% Font configuration
% ========================================

\usepackage[T1]{fontenc}
\usepackage{xltxtra,fontspec,xunicode}
\setsansfont{Arial}
\setmainfont{Calibri}
\newfontfamily{\monofont}{Source Code Pro}
\newfontfamily{\arial}{Arial}
\newfontfamily{\calibri}{Calibri}
\newfontfamily{\timesroman}{Times New Roman}
\newcommand*{\chapterfont}{\arial}


% Paragraph setup
% ========================================

% Set parkskip
% Don't remove parindent
% Don't affect ToC
\usepackage[skip=1em, indent]{parskip}

\usepackage{tocloft}
\setlength{\cftaftertoctitleskip}{10pt}
\setlength{\cftfignumwidth}{3em}
\setlength{\cfttabnumwidth}{3em}

\setlength\emergencystretch{.5\textwidth}


% Section setup
% ========================================

\usepackage{titlesec}
\usepackage{etoolbox}

\renewcommand\thechapter{\arabic{chapter}}
\ifdefined\sectionprefix
	\renewcommand{\thesection}{\sectionprefix{} \arabic{section}}
	\renewcommand\thesubsection{\alph{subsection})}
	\renewcommand\thesubsubsection{\roman{subsubsection}.}
\else
	\makeatletter
	\patchcmd{\chapter}{\if@openright\cleardoublepage\else\clearpage\fi}{}{}{}
	\renewcommand\@seccntformat[1]{\csname the#1\endcsname\quad}
	\makeatother
\fi
\renewcommand\theparagraph{\thesubsubsection.\arabic{paragraph}}

\titleformat{\chapter}[hang]{\fontsize{14}{16}\bfseries\sffamily}{\thechapter}{1em}{}
\titleformat*{\section}{\fontsize{12}{24}\bfseries\sffamily}
\titleformat*{\subsection}{\fontsize{12}{18}\itshape\sffamily}
\titleformat*{\subsubsection}{\fontsize{12}{18}\sffamily}
\titleformat{\paragraph}{\fontsize{12}{18}\sffamily}{\theparagraph}{1em}{}


\titlespacing{\chapter}{0pt}{0pt}{0pt}
% \titlespacing*{\section}{0pt}{24pt}{12pt}
% \titlespacing*{\subsection}{0pt}{6pt}{6pt}
% \titlespacing*{\subsubsection}{0pt}{12pt}{6pt}
\titlespacing*{\paragraph}{0pt}{12pt}{6pt}


% Preliminary pages
% ========================================

% Blank page at the beginning of document
\newcommand*\NewPage{\newpage\null\thispagestyle{empty}\newpage}

% Preamble
\newcommand*{\gpppipreamble}[0]{%
  \NewPage
  \pagenumbering{roman}
  \setcounter{page}{1}
  \tableofcontents
  \clearpage
  \listoftables
  \clearpage
  \listoffigures
  \clearpage
  \pagenumbering{arabic}
  \setcounter{page}{1}
}

\newenvironment{gpppipreambleenv}
{
	\NewPage
	\pagenumbering{roman}
	\setcounter{page}{1}
}
{
	\tableofcontents
	\clearpage
  	\listoftables
	\clearpage
	\listoffigures
	\clearpage
	\pagenumbering{arabic}
	\setcounter{page}{1}
}



% Liste des figures
\renewcommand{\listfigurename}{Liste des figures}


% References
% ========================================

\usepackage[square, numbers]{natbib}
\usepackage{IEEEtrantools}
\usepackage{url}
\usepackage[hidelinks]{hyperref}

% Section number in bibliography
\usepackage[numbib, nottoc]{tocbibind}

% Changement du nom de la section bibliographique, avec Babel
\addto{\captionsfrench}{\renewcommand{\bibname}{Références}}

\newcommand*{\gpppireferences}[0]{%
	\clearpage
	\bibliographystyle{IEEEtran-francais}
	\bibliography{References}
}


% Table of Contents
% ========================================

% No interference between lists of figures / tables and references
\usepackage{notoccite}

% Appendix
\usepackage[titletoc]{appendix}
\renewcommand{\appendixname}{Annexe}
% \renewcommand{\appendixpagename}{}

% Lists
% ========================================

\usepackage{enumitem}


% Math / numbers
% ========================================

% Units and such
\usepackage[binary-units=true]{siunitx}
\usepackage{amsmath}
\usepackage{mathtools}

% Matrices
% \usepackage{arydshln}

% Strikethroughs
\usepackage{cancel}

% Virgules décimales et formattage des nombres en français
\sisetup{group-separator = {\ }, output-decimal-marker = {,}, detect-inline-weight = text}

% Setup common operators and units
\DeclareSIUnit\rpm{RPM}
\DeclareSIUnit\bit{bit}
\DeclareSIUnit\bits{bits}
\DeclareSIUnit\baud{baud}
\DeclareSIUnit\pixel{px}
\DeclareSIUnit\image{image}
\DeclareSIUnit[number-unit-product = {\space}]{\inch}{po}
\newcommand{\mm}{\milli\meter}
\DeclareMathOperator{\sinc}{sinc}
\DeclarePairedDelimiter\abs{\lvert}{\rvert}

% Math commands
\newcommand{\LT}[2][]{\mathcal{L}^{#1}\left\{#2\right\}}

% cdot for products, slash for fractions
\sisetup{per-mode=symbol,inter-unit-product=\ensuremath{\cdot}}

% Laplace & Fourier
\usepackage[scr]{rsfso}
\newcommand{\Laplace}[1]{\mathscr{L} \left\{#1\right\}}
\newcommand{\LaplaceInverse}[1]{\mathscr{L}^{-1} \left\{#1\right\}}
\newcommand{\Fourier}[1]{\mathcal{F} \left\{#1\right\}}
\newcommand{\FourierInverse}[1]{\mathcal{F}^{-1} \left\{#1\right\}}


% Electrical Engineering Functions
% ========================================

% Phasors
\usepackage{steinmetz}

% I²C
\usepackage{xspace}
\newcommand{\ItwoC}{I\textsuperscript{2}C\xspace}


% Figures
% ========================================

\usepackage{graphicx}
\usepackage{float}
\usepackage[justification=justified,singlelinecheck=false]{caption}
\usepackage{subcaption}

\captionsetup[figure]{name={Figure}, labelsep = period}
\captionsetup[table]{name={Tableau}, labelsep = period}
\captionsetup[subfigure]{justification=centering}

% Code extract "figures"
\usepackage{listings}

% ===== EXEMPLE DE LSTSET =====
% \lstset{
%   language=octave,
%   basicstyle=\ttfamily\small,
%   numberstyle=\footnotesize,
%   numbers=left,
%   backgroundcolor=\color{lightgray!10},
%   frame=lines,
%   tabsize=2,
%   rulecolor=\color{black!30},
%   escapeinside={\%*}{*)},
%   breaklines=true,
%   breakatwhitespace=true,
%   framextopmargin=2pt,
%   framexbottommargin=2pt,
%   inputencoding=utf8,
%   extendedchars=true,
%   literate={á}{{\'a}}1 {ã}{{\~a}}1 {é}{{\'e}}1,
% }


% Syntax highlighted listings
% ========================================

\usepackage[cache=false]{minted}
\usepackage[dvipsnames]{xcolor}
\definecolor{LightGray}{gray}{0.9}
\newmintedfile[pythonscript]{python}{
  frame=lines,
  bgcolor=LightGray,
  linenos,
  breaklines
}
\newmintedfile[cppscript]{cpp}{
  frame=lines,
  bgcolor=LightGray,
  linenos,
  breaklines
}
\newmintedfile[matlabscript]{octave}{
  frame=lines,
  bgcolor=LightGray,
  linenos,
  breaklines
}
\newcommand{\mintedspace}{\vspace{-7mm}}
\newcommand{\code}[2][bgcolor=LightGray]{\mintinline[#1]{python}{#2}}

% MATLAB syntax highlighting
\usepackage{matlab-prettifier}


% Code output
% ========================================

% Code output
\usepackage{fancyvrb}

\RecustomVerbatimCommand{\VerbatimInput}{VerbatimInput}%
{fontsize=\footnotesize, %
 frame=lines,  % top and bottom rule only
 rulecolor=LightGray
}


% Plots
% ========================================

\usepackage{pgfplots}
\pgfplotsset{compat=1.15}


% Circuits
% ========================================

\usepackage[americanvoltages, americancurrents, fulldiodes,siunitx]{circuitikz}
\usetikzlibrary{shapes,arrows,chains,positioning,calc,patterns,angles,quotes}


% Digital timing diagrams / Chronogrammes
% ========================================

\usepackage{tikz-timing}


% Block diagrams
% ========================================

\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{calc,positioning}

\newcommand{\TikzSuma}{\tiny$+$}
\newcommand{\TikzSubs}{\tiny$-$}
\newcommand{\TikzMult}{$\times$}
\newcommand{\TikzInte}{$\displaystyle \int$}
\newcommand{\TikzDerv}[1]{$\frac{d \text{#1}}{dt}$}


% Flowcharts
% ========================================

\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]


% Tables
% ========================================

\usepackage{tabu}
\usepackage{tabularx}
\usepackage{makecell}
\newcommand{\tabitem}{~~\llap{\textbullet}~~}

\newcommand{\tabsubitem}[1]{\multicolumn{1}{@{\makebox[3em][c]{}} @{\makebox[2em][l]{~\textendash}} X}{#1}}

% New Column Types
\newcolumntype{L}{>{\raggedright\arraybackslash}X}%
\newcolumntype{C}{>{\centering\arraybackslash}X}%
\newcolumntype{R}{>{\raggedleft\arraybackslash}X}%

\newcommand{\rowgroup}[1]{\hspace{-1em}\textbf{#1}}

% Spacing in tables
\renewcommand*{\arraystretch}{1.5}

% Table rules
\usepackage{booktabs}

% Multiple pages table
\usepackage{longtable}


% Title page
% ========================================

\usepackage{multicol}
\setlength{\columnseprule}{4pt}
\def\columnseprulecolor{\color{black}}

\newcommand*{\gpppititlepage}[7]{%
	\begin{titlepage}
		\centering

		\begin{multicols*}{2}{\calibri
			#1 \par
			#2
			\vfill
			#3
			\vfill
			#4
			\vfill
			par \par
			#5
			\vfill
			Présenté à \par
			#6

			\vfill

			\par
			#7

            \columnbreak
            % \raggedright

			\includegraphics[height=0.30\textwidth]{\relativepath/umoncton-logo.png}\par\vspace{1cm}

			\null \vfill

			{\huge FACULTÉ \\\vspace{5mm} D'INGÉNIERIE}

			\vspace{1cm}

			{\LARGE UNIVERSITÉ \\ DE \vspace{3mm}\\ MONCTON}

			\vspace{1cm}

			{\large Moncton, NB, Canada}

			\vfill
			\vfill

		}\end{multicols*}
	\end{titlepage}
}


% Annexes
% ========================================
\usepackage{pdfpages}

% Footer
% ========================================

\usepackage{fancyhdr}
\pagestyle{fancy}

% Remove rule in header
\renewcommand{\headrulewidth}{0pt}

% Remove default header text
\lhead{}
\chead{}
\rhead{}

% Add page number to center of footer
\cfoot{-- \text{ }\thepage\text{ } --}
\usepackage{csquotes}

\preto\tabular{\setcounter{magicrownumbers}{0}}
\newcounter{magicrownumbers}
\newcommand\rownumber{\stepcounter{magicrownumbers}\arabic{magicrownumbers}}
